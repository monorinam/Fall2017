function fx = f(x)
fx = 1 ./ (1 + 25 * (x .^ 2));
end