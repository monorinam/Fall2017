function erfx = erf_func(x)
erfx = exp(-(x.^2))*(2/sqrt(pi));
end