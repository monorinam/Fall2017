1. To compile the files using test
    - Use TEST1 = 1 make : for to run sfs_test.c
    - Use TEST2 = 1 make: to run sfs_test2.c
    - Use make: to run fuse test
2. The executable is named Monorina_Mukhopadhyay_sfs
3. The disk file is named: mukhopadhyay_monorina.disk
4. Tested to pass all three tests on mimi.cs.mcgill.ca
5. To run fuse: use ./Monorina_Mukhopadhyay_sfs foldername
6. All valgrind errors come from rand_name: which is unmodified
